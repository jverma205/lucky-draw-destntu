# lucky-draw-DestNTU
The implemetation for the lucky draw webpage is going to be very simple.

1. Getting the registration IDs from the ERS excel file and keeping the name and reg Ids.
2. Creation of gif file using the url: https://gif.splendith.com/?fbclid=IwAR0Lvm1f2jlO_UapjeGFGxjsdBNPPA36dCfWvcbZX86ZRUxogRyQHcGklZI  
3. Cropping the gif file.
4. Converting the gif file into a horizontal-stacked Sprite file (png) so that it can be animated using CSS sprites. Useful url: https://ezgif.com/gif-to-sprite/ezgif-2-06d4cc9d5b1d.gif
5. Noting down the pixel size of the sprite file. Adding it to the css file named 'styles.css' as I did in this project.
6. Finally, display the webpage to the audience. Click on the Play/Pause button below the gif file to stop and see the lucky winner. Repeat the process unless 8 winners are chosen.

The url for gif creation requires us to add the list of items (regIDs and names) to dedicate its each frame. As soon as we get a regId from a student we can add it to the list.
This will not take much time. I added 50 items in 3 min. As soon as the gif is downloaded, we just need to add it in the html file. 

Following is a url which I found useful to use the CSS sprites method so that we can animate a gif file after conversion into sprites and control its Play/Pause state.
https://stackoverflow.com/a/45040337/5295723

We need to furnish the html file using css and javascript.